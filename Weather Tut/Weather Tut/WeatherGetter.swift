//
//  WeatherGetter.swift
//  Weather Tut
//
//  Created by Fernando Balandran on 7/28/17.
//  Copyright © 2017 Kodaman. All rights reserved.
//
import Foundation

// MARK: WeatherGetterDelegate
protocol WeatherGetterDelegate {
    func didGetWeather(weather: Weather)
    func didNotGetWeather(error: NSError)
}

class WeatherGetter {
    
    private let openWeatherMapBaseURL = "http://api.openweathermap.org/data/2.5/weather"
    private let openWeatherMapAPIKey = "e9a4475b613354b72564d42c93bf5517"
    
    private var delegate: WeatherGetterDelegate
    
    // MARK: -
    
    init(delegate: WeatherGetterDelegate) {
        self.delegate = delegate
    }
    
    func getWeatherByCity(city: String){
        let weatherRequestURL = URL(string: "\(openWeatherMapBaseURL)?APPID=\(openWeatherMapAPIKey)&q=\(city)")!
        getWeather(weatherRequestURL: weatherRequestURL)

    }
    
    private func getWeather(weatherRequestURL: URL){
        
            let task = URLSession.shared.dataTask(with: weatherRequestURL, completionHandler: { (data, response, error) in
                
                if let networkError = error {
                    
                    self.delegate.didNotGetWeather(error: networkError as NSError)
                    
                } else {
                    if let usableData = data {
                        print("Usable data size:\n\(usableData)\n")
                        
                        do{
                            let weatherJsonData = try JSONSerialization.jsonObject(with: usableData, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                            
                            let weather = Weather(weatherData: weatherJsonData)
                            
                            self.delegate.didGetWeather(weather: weather)
                            
                        } catch let jsonError as NSError{
                            // If an error occurs while converting data
                            self.delegate.didNotGetWeather(error: jsonError)
                        }
                    }
                }
            })
            
            task.resume()
        
    }
}
