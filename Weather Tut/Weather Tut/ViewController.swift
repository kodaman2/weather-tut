//
//  ViewController.swift
//  Weather Tut
//
//  Created by Fernando Balandran on 7/28/17.
//  Copyright © 2017 Kodaman. All rights reserved.
//

import UIKit

class ViewController: UIViewController, WeatherGetterDelegate, UITextFieldDelegate {
    
    @IBOutlet var currentTempLabel: UILabel!
    @IBOutlet var feelingDescriptionLabel: UILabel!
    @IBOutlet var overallDescriptionLabel: UILabel!
    @IBOutlet var windLabel: UILabel!
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var getCityWeatherButton: UIButton!
    
    
    var weather : WeatherGetter!
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weather = WeatherGetter(delegate: self)
        
        // Init UI
        currentTempLabel.text = ""
        feelingDescriptionLabel.text = ""
        overallDescriptionLabel.text = ""
        windLabel.text = ""
        cityTextField.text = ""
        cityTextField.placeholder = "Enter city name"
        cityTextField.delegate = self
        cityTextField.enablesReturnKeyAutomatically = true
        getCityWeatherButton.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Events
    // ---------------------
    
    @IBAction func getWeatherForCityButtonTapped(_ sender: UIButton) {
        guard let text = cityTextField.text, !text.isEmpty else{
            return
        }
        weather.getWeatherByCity(city: cityTextField.text!.urlEncoded)
        
    }
    
    // MARK: -
    
    // MARK: WeatherGetterDelegate methods
    // -----------------------------------
    
    func didGetWeather(weather: Weather) {
        
        // Asynchronous
        DispatchQueue.global(qos: DispatchQoS.userInitiated.qosClass).async {
            
            DispatchQueue.main.async {
                self.currentTempLabel.text = "\(weather.tempFahrenheit)"
                self.feelingDescriptionLabel.text = weather.weatherDescription
                self.windLabel.text = "Wind speed \(weather.windSpeed), wind direction \(weather.windDirection ?? 0.0)"
            }
        }
        
    }
    
    func didNotGetWeather(error: NSError) {
        
        // Asynchronous
        DispatchQueue.global(qos: DispatchQoS.userInitiated.qosClass).async {
        
            DispatchQueue.main.async  {
                self.showSimpleAlert(title: "Can't get the weather",
                                 message: "The weather service isn't responding.")
            }
            print("didNotGetWeather error: \(error)")
        }
    }
    
    // MARK: -
    
    // MARK: UITextFieldDelegate and related methods
    // ---------------------------------------------
    
    // Enable the "Get weather for the city above" button
    // If the city text field contains any text, disable it otherwise
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        getCityWeatherButton.isEnabled = prospectiveText.characters.count > 0
        print("Count: \(prospectiveText.characters.count)")
        
        return true
    }
    
    // Pressing the clear button on the text field
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        
        getCityWeatherButton.isEnabled = false
        return true
    }
    
    // Pressing the return button on teh keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        getWeatherForCityButtonTapped(getCityWeatherButton)
        return true
    }
    
    // Tapping on the view should dismiss the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    // MARK: Utility Methods
    
    func showSimpleAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }

}

extension String {
    // A method for encoding strings containing spaces and other charachters that
    // need to be converted for use in URL's
    var urlEncoded: String{
        return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlUserAllowed)!
    }
}

